This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Installation 

Install latest version of Node.js

Install everything:

### `npm install`

Install without Cypress:

### `npm install --no-optional`

Build:

### `npm run build`

Start development:

### `npm start`

Change relative path:

File: 

### `.env`

### `PUBLIC_URL=path`

Runs the app in the development mode.<br>
Open [http://localhost:4000](http://localhost:4000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

## Testing - Cypress

https://docs.cypress.io/guides/getting-started/installing-cypress.html#System-requirements 
