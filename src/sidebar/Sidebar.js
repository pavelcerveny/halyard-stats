import React from "react";
import { withRouter, matchPath } from 'react-router-dom';
import { Link } from "react-router-dom";
import './sidebar.scss';

function match(props) {
    return matchPath(props.history.location.pathname, {
        path: '/dashboard/:pageOrName?/:name?',
        exact: true,
        strict: false
    }).params;
}

function orderLinks(props) {
    return Object.keys(props.links).reverse().sort(link => {
        const data = props.links[link];
        if (data.url && props.location.pathname.includes(data.url)) {
            return -1;
        } else {
            return 1;
        }
    });
}

function Sidebar(props) {
    return (
        <aside>
            <div className="aside-brand">
                <Link to="/">halyard stats</Link>
            </div>
            <div className="aside-menu-wrapper">
                <div className="aside-menu">
                    <ul>
                        {orderLinks(props).map(link => {
                            const data = props.links[link];
                            let selected = 'menu-link';
                            let path = data.path;

                            if (props.location.pathname === '/' && data.path === '/') {
                                selected = 'menu-link-selected';
                            } else if (props.location.pathname.includes(data.path) && data.path !== '/') {
                                selected = 'menu-link-selected';
                            }

                            if (data.url) {
                                if (!props.location.pathname.includes(data.url)) {
                                    return "";
                                } else {
                                    const matchedParams = match(props);

                                    if (!matchedParams.name) {
                                        path += '/' + matchedParams.pageOrName;
                                    } else {
                                        if (matchedParams.page && data.url.includes(matchedParams.pageOrName)) {
                                            path += '/' + matchedParams.pageOrName + '/' + matchedParams.name;
                                        } else {
                                            if (data.path === '/dashboard') {
                                                selected = 'menu-link';
                                            }
                                            path += '/' + matchedParams.name;
                                        }
                                    }
                                }
                            }

                            return (
                                <li key={data.name}>
                                    <Link to={path} className={selected}>
                                        <i className={`${data.icon} menu-link-icon`} />
                                        <span className={`menu-link-title`}>{data.name}</span>
                                    </Link>
                                </li>
                            );
                        })}
                    </ul>
                </div>
            </div>
        </aside>
    );
}

export default withRouter(Sidebar);