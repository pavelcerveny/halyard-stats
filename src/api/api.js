import {
    searchForNamedGraphsQuery,
    getAllNamespaces,
    getTopTwenty,
    searchMultipleTypes,
    searchInSection,
    loadAllMetadataQuery, getClassesCount, getPropertiesCount, getObjectsCount, getVocabulariesForGraph
} from "./sparql";
import {SparqlJsonParser} from "sparqljson-parse";
import { matchPrefixAndReturnColour } from '../api/utils';
import ApiClient from "./client";

const sparqlJsonParser = new SparqlJsonParser();

export async function load(endpointUrl) {
    const endpoint = new ApiClient(endpointUrl);

    try {
        const response = await endpoint.selectQuery(searchForNamedGraphsQuery);
        const body = await response.text();
        const result = JSON.parse(body);

        if (result.boolean) {
            const metadata = await getDatasetMetadata(endpoint);
            const namespaces = await getDatasetNamespaces(endpoint);
            const subjects = await getDatasetTopTwentySubjects(endpoint);
            const objects = await getDatasetTopTwentyObjects(endpoint);
            const properties = await getDatasetTopTwentyProperties(endpoint);
            const counts = await getObjectCounts(endpoint);

            return {
                dataset: {
                    metadata,
                    namespaces,
                    subjects,
                    objects,
                    properties,
                    counts,
                },
                error: false,
                errorMsg: null,
                errorData: null,
            }
        } else {
            return {
                error: true,
                errorMsg: new Error("Api load: No statsContext inside dataset"),
                errorData: null,
            }
        }
    } catch (e) {
        return {
            error: true,
            errorMsg: new Error("Api load: Invalid SPARQL endpoint"),
            errorData: e,
        };
    }

}

export async function matchNamespaceIRI(namespaces) {

    try {
        const updatedNamespaces = Promise.all(namespaces.map(async (namespace) => {
            const match = await matchPrefixAndReturnColour(namespace.objectNamespace.value);
            if (match) {
                return {
                    ...match,
                    triples: namespace.tripleSum.value,
                    entities: namespace.entities.value,
                };
            }
            return null;
        }));

        let prefixedNamespaces = await updatedNamespaces;
        return prefixedNamespaces.filter(namespace => namespace);
    } catch (e) {  
        console.debug(e);
        return {
            error: true,
            errorMsg: new Error("Api - matchNamespaceIRI: Could not match given IRI and return colour"),
            errorData: e,
        };
    }
}


async function getDatasetMetadata(endpoint) {
    try {
        const response = await endpoint.selectQuery(loadAllMetadataQuery);
        const body = await response.text();
        const result = JSON.parse(body);
        return sparqlJsonParser.parseJsonResults(result);
    } catch (e) {
        return {
            error: true,
            errorMsg: new Error("Api - getDatasetMetadata: Error loading dataset metadata"),
            errorData: e,
        };
    }
}

async function getDatasetNamespaces(endpoint) {
    try {
        const response = await endpoint.selectQuery(getAllNamespaces);
        const body = await response.text();
        const result = JSON.parse(body);
        return sparqlJsonParser.parseJsonResults(result);
    } catch (e) {
        return {
            error: true,
            errorMsg: new Error("Api - getDatasetNamespaces: Error loading dataset namespaces"),
            errorData: e,
        };
    }
}



async function getDatasetTopTwenty(endpoint, type) {
    try {
        const response = await endpoint.selectQuery(getTopTwenty(type));
        const body = await response.text();
        const result = JSON.parse(body);
        return sparqlJsonParser.parseJsonResults(result);
    } catch (e) {
        return {
            error: true,
            errorMsg: new Error("Api - getDatasetTopTwenty: Error loading dataset top twenty"),
            errorData: e,
        };
    }
}

async function getDatasetTopTwentySubjects(endpoint) {
    return getDatasetTopTwenty(endpoint, 'subject');
}

async function getDatasetTopTwentyObjects(endpoint) {
    return getDatasetTopTwenty(endpoint, 'object');
}

async function getDatasetTopTwentyProperties(endpoint) {
    return getDatasetTopTwenty(endpoint, 'property');
}


export function createSparqlHttp(endpointUrl) {
    return new ApiClient(endpointUrl);
}

export async function search(endpointUrl, types, search, limit = null, offset = null, graph = null) {

    const endpoint = new ApiClient(endpointUrl);

    try {
        const response = await endpoint.selectQuery(searchMultipleTypes(types, search, limit, offset, graph));
        const body = await response.text();
        const result = JSON.parse(body);
        return sparqlJsonParser.parseJsonResults(result);
    } catch (e) {
        return {
            error: true,
            errorMsg: new Error("Api - search: Error searching in dataset"),
            errorData: e,
        };
    }
}

export async function searchInSections(endpointUrl, section, search, offset = null, limit = null) {

    const endpoint = new ApiClient(endpointUrl);

    try {
        const response = await endpoint.selectQuery(searchInSection(section, search, offset, limit));
        const body = await response.text();
        const result = JSON.parse(body);
        return sparqlJsonParser.parseJsonResults(result);
    } catch (e) {
        return {
            error: true,
            errorMsg: new Error("Api - searchInSections: Error searching in dataset sections"),
            errorData: e,
        };
    }
}

export async function getGraphVocabs(endpointUrl, graph) {
    const endpoint = new ApiClient(endpointUrl);

    try {
        const response = await endpoint.selectQuery(getVocabulariesForGraph(graph));
        const body = await response.text();
        const result = JSON.parse(body);
        return sparqlJsonParser.parseJsonResults(result);
    } catch (e) {
        return {
            error: true,
            errorMsg: new Error("Api - getGraphVocabs: Error getting graph vocabulary"),
            errorData: e,
        };
    }

    // return await performAjaxRequest(endpoint, getVocabulariesForGraph, graph);
}

async function getObjectCounts(endpoint) {

    let classesCnt = await request(endpoint, getClassesCount);
    classesCnt = classesCnt[0].entities.value;

    let propertiesCnt = await request(endpoint, getPropertiesCount);
    propertiesCnt = propertiesCnt[0].entities.value;

    let objectsCnt = await request(endpoint, getObjectsCount);
    objectsCnt = objectsCnt[0].entities.value;

    return {
        classesCnt,
        propertiesCnt,
        objectsCnt
    };
}

async function request(endpoint, sparql, ...params) {

    try {
        let response = {};
        if (typeof sparql === "function") {
            response = await endpoint.selectQuery(sparql(params));
        } else {
            response = await endpoint.selectQuery(sparql);
        }
        const body = await response.text();
        const result = JSON.parse(body);
        return sparqlJsonParser.parseJsonResults(result);
    } catch (e) {
        return {
            error: true,
            errorMsg: new Error("Api - request: Error during HTTP request"),
            errorData: e,
        };
    }
}
