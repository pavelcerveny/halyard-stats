import React, { Component } from 'react';
import {SparqlJsonParser} from "sparqljson-parse";
import './editor.scss';
const sparqlJsonParser = new SparqlJsonParser();
let datasetData = {};

class Editor extends Component {

    generateAutocompleteData = (sparqlRes) => {
        let data = sparqlRes.map(item => {
            return item.object.value;
        });

        let dataWithPrefixes = [];
        sparqlRes.map(item => {
            return datasetData.metadata.namespaces.forEach(namespace => {
                if (item.object.value.includes(namespace.iri)) {
                    dataWithPrefixes.push(`${namespace.prefix}:${item.object.value.substr(namespace.iri.length)}`);
                }
                return null;
            });
        });

        return data;
    };

    componentDidMount = () => {
        const that = this;
        let dataset = localStorage.getItem(`dataset:${this.props.match.params.name}`);
        dataset = datasetData = JSON.parse(dataset);
        const endpoint = dataset.endpoint;
        const namespaces = dataset.metadata.namespaces;
        this.setState({
            dataset
        });
        let prefixes = '';
        namespaces.forEach(namespace => {
            prefixes += `PREFIX ${namespace.prefix}: <${namespace.iri}> \n`;
        });

        /* global YASGUI */
        YASGUI.YASQE.defaults.value = `PREFIX void: <http://rdfs.org/ns/void#>
PREFIX halyard: <http://merck.github.io/Halyard/ns#>
PREFIX void-ext: <http://ldf.fi/void-ext#>
${prefixes}
SELECT ?pred ?obj
    WHERE {
        GRAPH halyard:statsContext {
            [] ?pred ?obj
        }
    }
LIMIT 10`;

        const customPropertyCompleter = function(yasqe) {
            //we use several functions from the regular property autocompleter (this way, we don't have to re-define code such as determining whether we are in a valid autocompletion position)
            const returnObj = {
                isValidCompletionPosition: function(){return YASGUI.YASQE.Autocompleters.properties.isValidCompletionPosition(yasqe)},
                preProcessToken: function(token) {return YASGUI.YASQE.Autocompleters.properties.preProcessToken(yasqe, token)},
                postProcessToken: function(token, suggestedString) {return YASGUI.YASQE.Autocompleters.properties.postProcessToken(yasqe, token, suggestedString)}
            };

            //In this case we assume the properties will fit in memory. So, turn on bulk loading, which will make autocompleting a lot faster
            returnObj.bulk = true;
            returnObj.async = true;

            //and, as everything is in memory, enable autoShowing the completions
            returnObj.autoShow = true;

            returnObj.persistent = "customProperties";//this will store the sparql results in the client-cache for a month.
            returnObj.get = function(token, callback) {
                //all we need from these parameters is the last one: the callback to pass the array of completions to
            const sparqlQuery = `PREFIX void: <http://rdfs.org/ns/void#>\nPREFIX halyard: <http://merck.github.io/Halyard/ns#>\nSELECT ?object ?triples {\n
	GRAPH halyard:statsContext {\n
    	?s a void:Dataset;\n
        	 void:triples ?triples;\n
             void:property ?object .\n
       }\n
    }\n
ORDER BY DESC(?triples)`;

                fetch(`${endpoint}?query=${encodeURIComponent(sparqlQuery)}`).then(function(response) {
                    return response.text();
                }).then(function(body) {
                    const result = JSON.parse(body);
                    const parsedResult = sparqlJsonParser.parseJsonResults(result);
                    callback(that.generateAutocompleteData(parsedResult));
                });
            };
            return returnObj;

        };
        //now register our new autocompleter
        YASGUI.YASQE.registerAutocompleter('customPropertyCompleter', customPropertyCompleter);

        YASGUI.defaults.catalogueEndpoints.unshift({endpoint, title: 'Dataset endpoint'});
        YASGUI.defaults.yasqe.sparql.endpoint = endpoint;
        YASGUI.YASQE.defaults.persistent = null;

        YASGUI(document.getElementById("editor"));
    };

    render() {
        return (
            <>
                <div className="content-sub-header">
                    <div className="left-side">
                        <h3 className="title">Properties</h3>
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xl-12">
                            <div className="card">
                                <div id="editor" />
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default Editor;
