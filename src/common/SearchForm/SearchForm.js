import React, {Component} from 'react';
import PropTypes from 'prop-types';

class SearchForm extends Component {

    generateNamespaceSelect = () => {
        let dataset = localStorage.getItem(`dataset:${this.props.match}`);
        dataset = JSON.parse(dataset).metadata.namespaces;
        dataset.sort((a, b) => b.triples - a.triples);
        let options = dataset.map(namespace => {
            return <option name={namespace.prefix} value={namespace.iri}
                           key={namespace.prefix}>{namespace.prefix}</option>
        });
        options.unshift(<option value="" key="none"/>);

        return options;
    };

    render() {
        return (
            <>
                <div className="col-lg-5">
                    <label>Namespace:</label>
                    <select
                        className='form-control'
                        placeholder="Choose namespace"
                        onChange={this.props.handleChangeNamespace}>
                        {this.generateNamespaceSelect()}
                    </select>
                </div>
                <div className="col-lg-5">
                    <label>Value:</label>
                    <input
                        className='form-control'
                        type="text"
                        placeholder="Enter searched text"
                        value={this.props.searchValue}
                        onChange={this.props.handleChangeSearchValue}/>
                </div>
                <div className="col-lg-2 column-bottom">
                    <button type="submit" className="btn btn-primary btn-up"
                            onClick={this.props.handleSubmitSearch}>Search
                    </button>
                </div>
            </>
        );
    }
}

SearchForm.propTypes = {
    handleChangeNamespace: PropTypes.func,
    handleChangeSearchValue: PropTypes.func,
    handleSubmitSearch: PropTypes.func,
    match: PropTypes.string,
};

export default SearchForm;
