import React from 'react';
import './spinner.scss';

const LoadingSpinner = () => (
    <div className="loader" />
);

export default LoadingSpinner;