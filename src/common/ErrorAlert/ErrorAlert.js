import React from 'react';
import PropTypes from 'prop-types';

function ErrorAlert({message, handleCloseAlert, deleteDataset, handleDeleteDataset}) {
    return (
        <div className="alert alert-danger" role="alert">
            <div className="alert-icon"><i className="fas fa-exclamation" /></div>
            <div className="alert-text">
                {message}
                {deleteDataset ?
                    <span style={{marginLeft: '2rem'}}>( Delete dataset ? <span style={{marginLeft: '0.4rem'}}/>
                    <i className="fa fa-trash"
                       style={{cursor: "pointer"}}
                       onClick={handleDeleteDataset}/> )
                </span>
                : ""}
            </div>
            <div className="alert-close">
                <i className="fa fa-times"
                   onClick={handleCloseAlert}/>
            </div>
        </div>
    );
}

ErrorAlert.propTypes = {
    message: PropTypes.string,
    handleCloseAlert: PropTypes.func,
    deleteDataset: PropTypes.bool,
    handleDeleteDataset: PropTypes.func
};

export default ErrorAlert;
