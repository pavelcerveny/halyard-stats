import React from "react";
import {BrowserRouter as Router, Redirect, Route, Switch} from "react-router-dom";
import './layout.scss';
import Sidebar from './sidebar/Sidebar';
import Dataset from './dataset/Dataset';
import Dashboard from "./dashboard/Dashboard";
import Compare from "./compare/Compare";
import Data from "./datasetSection/Data";
import Vocabulary from "./vocabulary/Vocabulary";
import NamedGraph from "./namedGraph/NamedGraph";
import Editor from "./editor/Editor";
import {datasetExists} from "./api/utils";

const AppRouter = () => (
    <Router basename={process.env.PUBLIC_URL}>
        <div className="wrapper">

            <Sidebar links={{
                compare: {
                    name: 'Compare datasets',
                    icon: 'fas fa-balance-scale',
                    path: '/compare',
                },
                datasets: {
                    name: 'Data sets',
                    icon: 'fas fa-database',
                    index: true,
                    path: '/'
                },
                dashboard: {
                    name: 'Dashboard',
                    icon: 'fas fa-chart-bar',
                    url: 'dashboard',
                    path: '/dashboard',
                },
                entities: {
                    name: 'Entities',
                    icon: 'fas fa-scroll',
                    url: 'dashboard',
                    path: '/dashboard/entities',
                },
                properties: {
                    name: 'Properties',
                    icon: 'fas fa-project-diagram',
                    url: 'dashboard',
                    path: '/dashboard/properties',
                },
                objects: {
                    name: 'Objects',
                    icon: 'fas fa-vector-square',
                    url: 'dashboard',
                    path: '/dashboard/objects',
                },
                vocabularies: {
                    name: 'Vocabularies',
                    icon: 'fas fa-atlas',
                    url: 'dashboard',
                    path: '/dashboard/vocabularies',
                },
                graphs: {
                    name: 'Named Graphs',
                    icon: 'fas fa-chart-pie',
                    url: 'dashboard',
                    path: '/dashboard/named-graphs',
                },
                editor: {
                    name: 'YASGUI',
                    icon: 'fas fa-edit',
                    url: 'dashboard',
                    path: '/dashboard/editor',
                },
            }}/>

            <div className="content-wrapper">
                <div className="content-header" />
                <div className="content">
                    <Switch>
                        <Route path="/" exact component={Dataset}/>
                        <Route path="/compare" exact component={Compare}/>
                        <Route path="/dashboard/entities/:name" component={datasetExists(Data)}/>
                        <Route path="/dashboard/properties/:name" component={datasetExists(Data)}/>
                        <Route path="/dashboard/objects/:name" component={datasetExists(Data)}/>
                        <Route path="/dashboard/vocabularies/:name" component={datasetExists(Vocabulary)}/>
                        <Route path="/dashboard/named-graphs/:name" component={datasetExists(NamedGraph)}/>
                        <Route path="/dashboard/editor/:name" component={datasetExists(Editor)}/>
                        <Route path="/dashboard/:name" exact component={datasetExists(Dashboard)}/>
                        <Route render={() => <Redirect to="/" />} />
                    </Switch>
                </div>
            </div>
        </div>
    </Router>
);

export default AppRouter;
