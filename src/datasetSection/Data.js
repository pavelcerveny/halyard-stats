import React, {Component} from 'react';
import {searchInSections} from "../api/api";
import {LoadingSpinner, SearchForm, ErrorAlert} from '../common';
import ReactPaginate from 'react-paginate';
import {getPrefixInSpanAndText} from "../api/utils";

const MAX_TEXT_LENGTH = 80;

function getSection(props) {
    if (props.location) {
        let pathnameSplit = props.location.pathname.split('/');
        return pathnameSplit[pathnameSplit.length - 2];
    }
}

class Data extends Component {

    state = {
        loading: false,
        searchValue: '',
        searchNamespace: '',
        searchResult: [],
        limit: 20,
        offset: 0,
        objCount: null,
        endpoint: '',
        datasetLoadFailed: null,
        section: null,
    };

    componentDidUpdate = (prevProps) => {
        if (getSection(this.props) !== getSection(prevProps)) {
            let dataset = localStorage.getItem(`dataset:${this.props.match.params.name}`);
            dataset = JSON.parse(dataset);
            const objCount = this.getObjectCount(dataset, getSection(this.props));
            this.setState({
                section: getSection(this.props),
                datasetLoadFailed: null,
                loading: true,
                objCount,
                offset: 0,
                limit: 20,
                searchValue: '',
                searchNamespace: '',
                searchResult: [],
            }, async () => {
                await this.callSearch(dataset.endpoint, '', this.state.offset, this.state.limit);
            });
        }
    };

    getObjectCount = (dataset, section) => {
        if (section === 'entities') {
            return dataset.metadata.counts.classesCnt;
        } else if (section === 'properties') {
            return dataset.metadata.counts.propertiesCnt;
        } else if (section === 'objects') {
            return dataset.metadata.counts.objectsCnt;
        }
    };

    componentDidMount = async () => {
        this.setState({
            loading: true
        });
        let dataset = localStorage.getItem(`dataset:${this.props.match.params.name}`);
        dataset = JSON.parse(dataset);
        const endpoint = dataset.endpoint;

        let objCount = this.getObjectCount(dataset, getSection(this.props));

        let data = await searchInSections(endpoint, getSection(this.props), this.state.searchValue, this.state.offset, this.state.limit);

        if (!data.error) {
            this.setState({
                searchResult: data,
                loading: false,
                objCount,
                endpoint,
                section: getSection(this.props),
            });
        } else {
            this.setState({
                datasetLoadFailed: data.errorMsg,
                loading: false,
                section: getSection(this.props),
            });
        }

    };


    handleChangeNamespace = (e) => {
        this.setState({
            searchNamespace: e.target.value
        });
    };

    handleChangeSearchValue = (e) => {
        this.setState({
            searchValue: e.target.value
        });
    };

    handleSubmitSearch = async (e) => {
        e.preventDefault();

        const dataset = localStorage.getItem(`dataset:${this.props.match.params.name}`);
        this.setState({
            loading: true
        });
        let data = await searchInSections(JSON.parse(dataset).endpoint, getSection(this.props), this.state.searchNamespace + this.state.searchValue);

        if (!data.error) {
            this.setState({
                searchResult: data.slice(0, 20),
                loading: false,
                offset: 0,
                objCount: data.length
            });
        } else {
            this.setState({
                datasetLoadFailed: data.errorMsg,
                loading: false,
            });
        }
    };

    generateTableItem = (data) => {
        return data.map((item, index) => {
            let text = item.object.value;
            let changedText = getPrefixInSpanAndText(text);

            text = changedText.text;
            let prefix = changedText.prefix;

            if (text.length > MAX_TEXT_LENGTH) {
                text = text.substr(0, MAX_TEXT_LENGTH) + '...';
            }
            let body = '';
            if (this.state.section === 'entities') {
                if (item.prop) {
                    body = (
                        <tr key={index}>
                            <td><span className="hint--bottom" aria-label={item.object.value}>{prefix}{text}</span></td>
                            <td>{item.prop.value.replace('http://ldf.fi/void-ext#', '')}</td>
                            <td className="font-sans-serif">{item.triples.value}</td>
                        </tr>
                    );
                } else {
                    body = (<tr key={index}>
                        <td><span className="hint--bottom" aria-label={item.object.value}>{prefix}{text}</span></td>
                        <td></td>
                        <td className="font-sans-serif">{item.triples.value}</td>
                    </tr>);
                }
            } else {
                body = (
                    <tr key={index}>
                        <td><span className="hint--bottom" aria-label={item.object.value}>{prefix}{text}</span></td>
                        <td className="font-sans-serif">{item.triples.value}</td>
                    </tr>
                );
            }
            return body;
        });
    };

    callSearch = async (endpoint, search = '', offset = null, limit = null) => {
        let data = await searchInSections(endpoint, getSection(this.props), search, offset, limit);

        if (!data.error) {
            this.setState({
                searchResult: data,
                loading: false,
            });
        } else {
            this.setState({
                datasetLoadFailed: data.errorMsg,
                loading: false,
            });
        }
    };

    paginateChange = async (offset) => {
        window.scrollTo(0, 0);
        this.setState({
            loading: true
        });
        const searchResult = await searchInSections(this.state.endpoint, getSection(this.props), this.state.searchValue, offset.selected, this.state.limit);

        this.setState({
            searchResult,
            loading: false,
            offset: offset.selected
        });
    };

    changeLimit = (e) => {
        this.setState({
            limit: e.target.value
        });
    };

    handleCloseAlert = () => {
        this.setState({
            datasetLoadFailed: null,
        });
    };


    render() {
        return (
            <>
                {this.state.datasetLoadFailed ?
                    <ErrorAlert
                        message={this.state.datasetLoadFailed.message}
                        handleCloseAlert={this.handleCloseAlert}
                    />
                    : ""
                }
                {this.state.loading ? <LoadingSpinner/> : ""}
                <div className="content-sub-header">
                    <div className="left-side">
                        <h3 className="title">{getSection(this.props).charAt(0).toUpperCase() + getSection(this.props).slice(1)}</h3>
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xl-12">
                            <div className="card">
                                <div className="card-head">
                                    <div className="card-head-label">
                                        <h3 className="title">Explore {getSection(this.props)} in
                                            - {this.props.match.params.name}</h3>
                                    </div>
                                </div>
                                <div className="card-body card-content-center">
                                    <div className="row">
                                        <SearchForm
                                            match={this.props.match.params.name}
                                            handleChangeNamespace={this.handleChangeNamespace}
                                            handleChangeSearchValue={this.handleChangeSearchValue}
                                            handleSubmitSearch={this.handleSubmitSearch}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row" style={{justifyContent: 'center'}}>
                        <div className="col-xl-12">
                            <div className="card">
                                <div className="card-head">
                                    <div className="card-head-label">
                                        <h3 className="title">Results</h3>
                                    </div>
                                </div>
                                <div className="card-body card-content-center">
                                    <table className="table">
                                        {this.state.section === 'entities' ?
                                            <colgroup>
                                                <col width="60%"/>
                                                <col width="20%"/>
                                                <col width="20%"/>
                                            </colgroup>
                                            :
                                            <colgroup>
                                                <col width="80%"/>
                                                <col width="20%"/>
                                            </colgroup>
                                        }
                                        <thead>
                                        {this.state.section === 'entities' ?
                                            <tr>
                                                <th>NAME</th>
                                                <th>TYPE</th>
                                                <th>TOTAL</th>
                                            </tr>
                                            :
                                            <tr>
                                                <th>NAME</th>
                                                <th>TOTAL</th>
                                            </tr>
                                        }
                                        </thead>
                                        <tbody>
                                        {this.state.searchResult.length > 0 ?
                                            this.generateTableItem(this.state.searchResult)
                                            : <tr>
                                                <td colSpan={4}>Nothing to display</td>
                                            </tr>}
                                        </tbody>
                                    </table>
                                </div>
                                {this.state.objCount && this.state.searchResult.length > 0 ?
                                    <div className="card-footer">
                                        <div className="pagination-box">
                                            <div className="col-xl-8">
                                                <ReactPaginate
                                                    onPageChange={this.paginateChange}
                                                    pageCount={Math.ceil(parseFloat(this.state.objCount / this.state.limit))}
                                                    pageRangeDisplayed={5}
                                                    marginPagesDisplayed={2}
                                                    containerClassName={'pagination'}
                                                    subContainerClassName={'pages pagination'}
                                                    activeClassName={'active'}
                                                    breakClassName={'page-link'}
                                                    pageClassName={'page-item'}
                                                    pageLinkClassName={'page-link'}
                                                    previousClassName={'page-item'}
                                                    nextClassName={'page-item'}
                                                    previousLinkClassName={'page-link'}
                                                    nextLinkClassName={'page-link'}/>
                                            </div>
                                            <div className="col-xl-4">
                                                <div className="content-to-end">
                                                    <select
                                                        value={this.state.limit}
                                                        onChange={this.changeLimit}
                                                    >
                                                        <option value={10}>10</option>
                                                        <option value={20}>20</option>
                                                        <option value={50}>50</option>
                                                    </select>
                                                    <span className="pagination-count">
                                                        {`${1 + (this.state.limit * this.state.offset)}
                                                        - ${(this.state.limit * (this.state.offset + 1)) > this.state.objCount ? this.state.objCount : (this.state.limit * (this.state.offset + 1))}
                                                        of ${this.state.objCount}`}
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    : ""
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default Data;
